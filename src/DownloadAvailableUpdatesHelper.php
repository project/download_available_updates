<?php

/**
* @file
* Contains Drupal\download_available_updates\DownloadAvailableUpdatesHelper
*/

namespace Drupal\download_available_updates;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

/**
*
*/
class DownloadAvailableUpdatesHelper
{

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }

  /**
   * Constructs a new UpdateManagerDownload.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  public function getAvailableUpdatesData() {
    if ($available = update_get_available(TRUE)) {
      $this->moduleHandler->loadInclude('update', 'compare.inc');
      $result = update_calculate_project_data($available);
    }
    return $result;
  }

  public function downloadExcelWorksheet($data) {
    // redirect output to client browser
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="Available-updates.xlsx"');
    header('Cache-Control: max-age=0');

    flush();

    $spreadsheetObj = $this->createExcelWorksheetData($data);

    $writer = new Xlsx($spreadsheetObj);

    ob_end_clean();
    $writer->save('php://output');
    exit();
  }

  protected function createExcelWorksheetData($data) {
    $spreadsheet = new Spreadsheet();
    //Add some data.
    $spreadsheet->setActiveSheetIndex(0);
    $worksheet = $spreadsheet->getActiveSheet();

    //Rename sheet.
    $worksheet->setTitle('Available updates');
    $worksheet->getColumnDimension('A')->setAutoSize(true);
    $worksheet->getColumnDimension('B')->setAutoSize(true);
    $worksheet->getColumnDimension('C')->setAutoSize(true);
    $worksheet->getColumnDimension('D')->setAutoSize(true);
    $worksheet->getColumnDimension('E')->setAutoSize(true);
    // Header.
    //Set Header style.
    $styleArray = [
      'font' => [
        'bold' => true,
      ],
      'alignment' => [
        'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
      ],
      'borders' => [
        'top' => [
          'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
        ],
      ],
      'fill' => [
        'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_GRADIENT_LINEAR,
        'rotation' => 90,
        'startColor' => [
          'argb' => 'FFA0A0A0',
        ],
        'endColor' => [
          'argb' => 'FFFFFFFF',
        ],
      ],
    ];
    $worksheet->getStyle('A1:E1')->applyFromArray($styleArray);
    $worksheet->setCellValue('A1', 'Title');
    $worksheet->setCellValue('B1', 'Type');
    $worksheet->setCellValue('C1', 'Existing Version');
    $worksheet->setCellValue('D1', 'Recommended Version');
    $worksheet->setCellValue('E1', 'Latest Version');
    // Data rows.
    $i = 2;
    foreach ($data as $key => $value) {
      $worksheet->setCellValue('A' . $i, $value['title']);
      $worksheet->setCellValue('B' . $i, $value['project_type']);
      $worksheet->setCellValue('C' . $i, $value['existing_version']);
      $worksheet->setCellValue('D' . $i, $value['recommended']);
      $worksheet->setCellValue('E' . $i, $value['latest_version']);

      $i++;
    }

    return $spreadsheet;
  }

}
