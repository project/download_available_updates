<?php

/**
* @file
* Contains Drupal\download_available_updates\Controller\DownloadAvailableUpdatesController
*/

namespace Drupal\download_available_updates\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\download_available_updates\DownloadAvailableUpdatesHelper;
use Drupal\Core\Url;


/**
 * Download available updates form.
 */
class DownloadAvailableUpdatesController extends ControllerBase {

  /**
   * The update helper.
   *
   * @var \Drupal\download_available_updates\DownloadAvailableUpdatesHelper
   */
  protected $updateHelper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('download_available_updates.helper')
    );
  }

  /**
   * Constructs a new UpdateManagerDownload.
   *
   * @param \Drupal\download_available_updates\DownloadAvailableUpdatesHelper $update_helper
   *   The update helper.
   */
  public function __construct(DownloadAvailableUpdatesHelper $update_helper) {
    $this->updateHelper = $update_helper;
  }

  // Using PHPSpreadsheet library.
  public function downloadAvailableUpdates() {
    // Checking the PHPSpreadsheet library classes exists or not.
    if (!class_exists('\PhpOffice\PhpSpreadsheet\Spreadsheet') && !class_exists('\PhpOffice\PhpSpreadsheet\Writer\Xlsx')) {
      drupal_set_message(t('PHPSpreadsheet library is not found in the system. Please check logs for further details.'), 'error');
      \Drupal::logger('download_available_updates')->error("PHPSpreadsheet library is missing. You can install it using composer. Run this command composer require phpoffice/phpspreadsheet");

      $url = Url::fromRoute('update.status');
      return $this->redirect($url->getRouteName());
    }
    else {
      $availableUpdatesData = $this->updateHelper->getAvailableUpdatesData();
      $this->updateHelper->downloadExcelWorksheet($availableUpdatesData);
    }
  }

}
