DOWNLOAD AVAILABLE UPDATES

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * How it works
 * Support requests

INTRODUCTION
------------

Download available updates module provides a way to download website's available updates of  modules and themes status report into an Excel file.

It adds to the site:
* A new url: /admin/reports/updates/download.

REQUIREMENTS
------------

This module requires the PHP ZIP extension must be enabled.


RECOMMENDED MODULES
-------------------

No any module is needed.


INSTALLATION
------------

 * Download PHPSpreadsheet library
   (https://github.com/PHPOffice/PhpSpreadsheet). We recommend to use composer to install the library.
   Run this command to install this library :-
    composer require phpoffice/phpspreadsheet

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No configuration is needed.


HOW IT WORKS
------------

The module adds the "Download Available Updates" button right next to the  "Install New Module Or Theme" button the site's update status page. Goto admin/reports/updates to find this button.

When the user clicks on "Download Availavle Updates" button, it automatically downloads the avaiable updates excel file.


SUPPORT REQUESTS
----------------

Before posting a support request, carefully read the installation
instructions provided in module documentation page.

Before posting a support request, check Recent log entries at
admin/reports/dblog

When posting a support request, please inform what does the status report say
at admin/reports/dblog and if you were able to see any errors in
Recent log entries.
